# -*- coding: utf-8 -*-

"""
setori.exceptions
~~~~~~~~~~~~~~~~~

This module contains the set of setori's exceptions.

"""


class TableNotFound(Exception):
    """
    """


class InvalidSongModel(Exception):
    """
    """


class InvalidActorModel(Exception):
    """
    """


class InvalidCharacterModel(Exception):
    """
    """


class InvalidEventModel(Exception):
    """
    """


class InvalidReleaseModel(Exception):
    """
    """


class InvalidSetListModel(Exception):
    """
    """


class InvalidTrackListModel(Exception):
    """
    """
