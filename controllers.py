# -*- coding: utf-8 -*-

"""
setori.controllers
~~~~~~~~~~~~~~~~~~

This module contains the set of setori's controllers.

"""

from flask import (Blueprint, request, render_template,
                   redirect, url_for, abort)
from app import cache
from . import (df_song,
               df_actr,
               df_char,
               df_event,
               df_release,
               df_song2actr2event,
               df_song2char2release)
from .models import (search_models,
                     get_model,
                     get_models,
                     Song,
                     Actor,
                     Character,
                     Event,
                     Release,
                     SetList,
                     TrackList)
from .exceptions import (InvalidSongModel,
                         InvalidActorModel,
                         InvalidCharacterModel,
                         InvalidEventModel,
                         InvalidReleaseModel)
from urllib.parse import urlencode
from pandas.tslib import NaT
from collections import OrderedDict
from datetime import date


setori = Blueprint('setori', __name__, url_prefix='/')


def get_sorted_setlists(df_s2a2e):
    """セットリストのDataFrameからソート済みのセットリストModelのリストを取得する。
    """
    setlists = get_models(df_s2a2e, SetList,
                          ['event', 'part_number', 'order'], sort=False)
    setlists = sorted(setlists, key=lambda x: x.order, reverse=False)
    setlists = sorted(setlists, key=lambda x: x.part_number, reverse=False)
    setlists = sorted(setlists, key=lambda x: x.event.name, reverse=False)
    setlists = sorted(setlists, key=lambda x: x.event.date, reverse=True)

    return setlists


def get_sorted_tracklists(df_s2c2r):
    """トラックリストのDataFrameからソート済みのトラックリストModelのリストを取得する。
    """
    tracklists = get_models(df_s2c2r, TrackList,
                            ['release', 'disc_number', 'order'], sort=False)
    tracklists = sorted(tracklists, key=lambda x: x.order, reverse=False)
    tracklists = sorted(tracklists, key=lambda x: x.disc_number, reverse=False)
    tracklists = sorted(tracklists, key=lambda x: x.release.name, reverse=True)
    tracklists = sorted(tracklists, key=lambda x: x.release.date, reverse=True)

    return tracklists


def get_songs_actors_characters_via_setlists_tracklists(setlists, tracklists,
                                                        sort_by='name_kana'):
    """セットリストとトラックリスト、それぞれのリストから、そのすべての曲・役者・キャラクターのリストを取得する。

    :param setlists: セットリスト
    :type setlists: list
    :param tracklists: トラックリスト
    :type tracklists: list
    :param sort_by: sortを行なうcolumn名
    :type sort_by: str
    """

    # セットリストとトラックリスト、それぞれの役者とキャラクターを取得する
    grouped_actors_dicts = [l.set_actors() for l in setlists]
    grouped_characters_dicts = [l.set_characters() for l in tracklists]

    # セットリストとトラックリストの曲名を取得する
    song_names = [l.song.name for l in setlists]
    song_names += [l.song.name for l in tracklists]

    # セットリストとトラックリストの役者名を取得する
    actor_names = [actor.name for grouped_actors_dict in grouped_actors_dicts
                   for actors in grouped_actors_dict.values()
                   for actor in actors]
    actor_names += [actor.name
                    for grouped_characters_dict in grouped_characters_dicts
                    for characters in grouped_characters_dict.values()
                    for character in characters
                    for actor in character.actors]

    # セットリストとトラックリストのキャラクター名を取得する
    character_names = [character.name
                       for grouped_characters_dict in grouped_characters_dicts
                       for characters in grouped_characters_dict.values()
                       for character in characters]
    character_names += [character.name
                        for grouped_actors_dict in grouped_actors_dicts
                        for actors in grouped_actors_dict.values()
                        for actor in actors
                        for character in actor.characters]

    # 曲・役者・キャラクター名のリストをModel化する
    songs = get_models(
        df_song[df_song[Song._primary_key].isin(song_names)],
        Song, ['name'], sort=True, sort_by=sort_by)
    actors = get_models(
        df_actr[df_actr[Actor._primary_key].isin(actor_names)],
        Actor, ['name'], sort=True, sort_by=sort_by)
    characters = get_models(
        df_char[df_char[Character._primary_key].isin(character_names)],
        Character, ['name'], sort=True, sort_by=sort_by)

    return songs, actors, characters


@setori.app_template_filter('split_space')
def split_space(s):
    return s.strip().split()


@setori.app_template_filter('flatten_list')
def flatten_list(list_of_lists):
    return [i for l in list_of_lists for i in l]


@setori.app_template_filter('drop_duplicates')
def drop_duplicates(li):
    return list(OrderedDict.fromkeys(li))


@setori.app_template_filter('datetime')
def datetimeformat(value, format='%Y.%m.%d'):
    if value is NaT:
        return ''
    else:
        return str(value.strftime(format)).replace('.0', '.')


@setori.app_template_filter('lremove')
def lremoveformat(value, l):
    if value.startswith(l):
        return value[len(l):].strip()
    else:
        return value


def validate_queries(queries, permitted_query_keys):
    """
    :param queries: ImmutableMultiDict
    :param list permitted_query_keys: list
    """
    if set(queries) != set(permitted_query_keys):
        abort(400)  # catch not corrected query key
    elif any(len(queries.getlist(key)) != 1 for key in permitted_query_keys):
        abort(400)  # catch duplicated query key


@setori.route('', methods=['GET'])
def index():
    song_length = len(df_song[df_song['type'] == ''])
    actor_length = len(df_actr)
    character_length = len(df_char)
    event_length = len(df_event[df_event['part_number'] == 1])
    release_length = len(df_release[df_release['disc_number'] == 1])

    df_e = df_event[df_event['date'] <= date.today()]
    df_r = df_release[df_release['date'] <= date.today()]

    events = get_models(df_e.drop_duplicates('name').sort(
                        ['date', 'name'], ascending=[False, False]).head(3),
                        Event, ['name'], sort=False)
    releases = get_models(df_r.drop_duplicates('name').sort(
                        ['date', 'name'], ascending=[False, False]).head(3),
                        Release, ['name'], sort=False)

    return render_template('setori/index.html',
                           song_length=song_length,
                           actor_length=actor_length,
                           character_length=character_length,
                           event_length=event_length,
                           release_length=release_length,
                           events=events,
                           releases=releases)


@setori.route('about', methods=['GET'])
def about():
    return render_template('setori/about.html')


@setori.route('contact', methods=['GET'])
def contact():
    return render_template('setori/contact.html')


@setori.route('search', methods=['GET'])
def search():
    validate_queries(request.args, ['o', 'q'])

    # not immutable dict for redirecting after
    request_args = request.args.to_dict()
    obj = request_args.pop('o')

    if obj == 'song':
        return redirect(url_for('.songs') + '?' + urlencode(request_args))
    elif obj == 'actor':
        return redirect(url_for('.actors') + '?' + urlencode(request_args))
    elif obj == 'character':
        return redirect(url_for('.characters') + '?' + urlencode(request_args))
    else:
        abort(400)
    return render_template('setori/index.html')


@setori.route('songs', methods=['GET'])
def songs():
    if request.args.get('q'):
        validate_queries(request.args, ['q'])
        searchword = request.args['q']

        songs = search_models(searchword, df_song, Song)
        if len(songs) == 0:
            abort(404)
        elif len(songs) == 1:
            return redirect(url_for('.song', name=songs[0].name))
        else:
            return render_template('setori/results.html', o='song',
                                   q=searchword,
                                   songs=songs)

    songs = get_models(df_song, Song, ['name'], sort_by='name_kana')
    return render_template('setori/songs.html', songs=songs)


@setori.route('songs/<name>', methods=['GET'])
@cache.cached(timeout=2592000)
def song(name):
    df_s = df_song[df_song['name'] == name]
    df_s2a2e = df_song2actr2event[df_song2actr2event['song'] == name]
    df_s2c2r = df_song2char2release[df_song2char2release['song'] == name]

    try:
        song = get_model(df_s, Song, ['name'])
    except (ValueError, InvalidSongModel):
        abort(404)
    setlists = get_sorted_setlists(df_s2a2e)
    tracklists = get_sorted_tracklists(df_s2c2r)
    (_, actors,
        characters) = get_songs_actors_characters_via_setlists_tracklists(
        setlists, tracklists, sort_by='name_kana')

    return render_template('setori/song.html', song=song,
                           setlists=setlists, tracklists=tracklists,
                           actors=actors, characters=characters)


@setori.route('actors', methods=['GET'])
def actors():
    if request.args.get('q'):
        validate_queries(request.args, ['q'])
        searchword = request.args['q']

        actors = search_models(searchword, df_actr, Actor)
        if len(actors) == 0:
            abort(404)
        elif len(actors) == 1:
            return redirect(url_for('.actor', name=actors[0].name))
        else:
            return render_template('setori/results.html', o='actor',
                                   q=searchword,
                                   actors=actors)

    actors = get_models(df_actr, Actor, ['name'],
                        sort_by='name_kana')
    [actor.set_characters([]) for actor in actors]
    return render_template('setori/actors.html', actors=actors)


@setori.route('actors/<name>', methods=['GET'])
@cache.cached(timeout=2592000)
def actor(name):
    df_a = df_actr[df_actr['name'] == name]
    df_s2a2e = df_song2actr2event[df_song2actr2event['actor'] == name]
    df_s2c2r = df_song2char2release[df_song2char2release['actor'] == name]

    try:
        actor = get_model(df_a, Actor, ['name'])
    except (ValueError, InvalidActorModel):
        abort(404)
    setlists = get_sorted_setlists(df_s2a2e)
    tracklists = get_sorted_tracklists(df_s2c2r)
    (songs, actors,
        characters) = get_songs_actors_characters_via_setlists_tracklists(
        setlists, tracklists, sort_by='name_kana')

    actor.set_characters([])
    return render_template('setori/actor.html', actor=actor,
                           setlists=setlists, tracklists=tracklists,
                           songs=songs, actors=actors, characters=characters)


@setori.route('characters', methods=['GET'])
def characters():
    if request.args.get('q'):
        validate_queries(request.args, ['q'])
        searchword = request.args['q']

        characters = search_models(searchword, df_char, Character)
        if len(characters) == 0:
            abort(404)
        elif len(characters) == 1:
            return redirect(url_for('.character', name=characters[0].name))
        else:
            return render_template('setori/results.html', o='character',
                                   q=searchword,
                                   characters=characters)

    characters = get_models(df_char, Character, ['name'],
                            sort_by='name_kana')
    [character.set_actors([]) for character in characters]
    return render_template('setori/characters.html', characters=characters)


@setori.route('characters/<name>', methods=['GET'])
@cache.cached(timeout=2592000)
def character(name):
    df_c = df_char[df_char['name'] == name]
    df_s2a2e = df_song2actr2event[df_song2actr2event['character'] == name]
    df_s2c2r = df_song2char2release[df_song2char2release['character'] == name]

    try:
        character = get_model(df_c, Character, ['name'])
    except (ValueError, InvalidCharacterModel):
        abort(404)
    setlists = get_sorted_setlists(df_s2a2e)
    tracklists = get_sorted_tracklists(df_s2c2r)
    (songs, actors,
        characters) = get_songs_actors_characters_via_setlists_tracklists(
        setlists, tracklists, sort_by='name_kana')

    character.set_actors([])
    return render_template('setori/character.html', character=character,
                           setlists=setlists, tracklists=tracklists,
                           songs=songs, actors=actors, characters=characters)


@setori.route('events', methods=['GET'])
@cache.cached(timeout=2592000)
def events():
    events = get_models(df_event, Event, ['name'], sort=False)
    [e.set_actors() for e in events]
    events = sorted(events, key=lambda x: x.name)
    events = sorted(events, key=lambda x: int(
            x.date.strftime('%Y%m%d')) if (x.date is not NaT) else 100000000,
        reverse=False)[::-1]
    return render_template('setori/events.html', events=events)


@setori.route('events/<name>', methods=['GET'])
@cache.cached(timeout=2592000)
def event(name):
    df_e = df_event[df_event['name'] == name]

    try:
        event = get_model(df_e, Event, ['name'])
    except (ValueError, InvalidEventModel):
        abort(404)
    event.set_actors()
    event.set_setlists()
    setlists = [setlist for part in event.parts
                for setlist in part['setlists']]
    (songs, actors,
        characters) = get_songs_actors_characters_via_setlists_tracklists(
        setlists, [], sort_by='name_kana')
    return render_template('setori/event.html', event=event,
                           songs=songs, actors=actors, characters=characters)


@setori.route('releases', methods=['GET'])
@cache.cached(timeout=2592000)
def releases():
    releases = get_models(df_release, Release, ['name'], sort=False)
    [r.set_characters() for r in releases]
    releases = sorted(releases, key=lambda x: x.name)
    releases = sorted(releases, key=lambda x: int(
            x.date.strftime('%Y%m%d')) if (x.date is not NaT) else 100000000,
        reverse=False)[::-1]
    return render_template('setori/releases.html', releases=releases)


@setori.route('releases/<name>', methods=['GET'])
@cache.cached(timeout=2592000)
def release(name):
    df_r = df_release[df_release['name'] == name]

    try:
        release = get_model(df_r, Release, ['name'])
    except (ValueError, InvalidReleaseModel):
        abort(404)
    release.set_characters()
    release.set_tracklists()
    tracklists = [tracklist for disc in release.discs
                  for tracklist in disc['tracklists']]
    (songs, actors,
        characters) = get_songs_actors_characters_via_setlists_tracklists(
        [], tracklists, sort_by='name_kana')

    return render_template('setori/release.html', release=release,
                           songs=songs, actors=actors, characters=characters)
